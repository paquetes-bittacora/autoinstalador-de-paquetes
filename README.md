# bPanel4 autoinstaller

Instala automáticamente los paquetes de bPanel4 compatibles.

## Configurar un paquete para que sea autoinstalable
Para que un paquete sea compatible, debe declarar sus comandos de instalación en su 
`composer.json` de forma parecida a como se hace con los Providers de Laravel.

Usando como ejemplo un paquete inventado, su `composer.json` debería incluir
lo siguiente:

```json
...
"extra": {
    ...
    "bpanel4": {
        "installers": [
            "paquete-inventado:install"
        ]
    }
}
...
```

Este paquete debería definir un comando de instalación con la siguiente `$signature`:

```php
protected $signature = 'paquete-inventado:install';
```

Ya no habría que hacer nada más, solo ejecutar el autoinstalador:

## Instalar paquetes automáticamente

Hay que ejecutar el comando:

```
php artisan bpanel4:install-packages
```

Los comandos de instalación que ya se ejecuten se guardarán en la base de datos, para evitar
que un comando se ejecute más de una vez. Esto es útil también cuando un paquete
vaya evolucionando, porque permite que cada versión incluya sus comandos de actualización. Por
ejemplo, ejecutarán las nuevas migraciones o seeders, publicarán assets, etc, sin que
los usuarios del paquete tengan que hacer nada manualmente (o muy poco).

Si un paquete necesita otros, el autoinstalador los reconocerá y ejecutará también
sus comandos de instalación, si se han definido como pone más arriba.

Esto se puede automatizar más modificando el `composer.json` del proyecto. Hay que modificar el apartado `post-autoload-dump` para añadir la línea `@php artisan bpanel4:install-packages`. Quedaría así: 

```json
"post-autoload-dump": [
    "Illuminate\\Foundation\\ComposerScripts::postAutoloadDump",
    "@php artisan package:discover --ansi",
    "@php artisan bpanel4:install-packages"
],
```

Al hacer este cambio, cada vez que instalemos o actualicemos un paquete de bpanel por composer, se ejecutarán automáticamente sus comandos de instalación.

## Importante

El funcionamiento de este paquete se basa en el archivo `composer.lock` del proyecto,
así que tendrá que estar actualizado antes de ejecutarlo. Habrá que ejecutar `composer update`
para los paquetes que tengan cambios, aunque se estén desarrollando en local, ya que si no
los cambios del `composer.json` del paquete no se pasarán al `composer.lock` global del
proyecto.
