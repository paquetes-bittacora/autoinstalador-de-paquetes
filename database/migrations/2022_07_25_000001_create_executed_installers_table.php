<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('bpanel4_executed_installers', static function (Blueprint $table) {
            $table->id();
            $table->string('command');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::drop('bpanel4_executed_installers');
    }
};
