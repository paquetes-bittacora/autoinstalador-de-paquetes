<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Autoinstaller\Commands;

use Artisan;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use RuntimeException;

class AutoinstallPackages extends Command
{
    private const TABLE = 'bpanel4_executed_installers';
    private const NUMBER_OF_RETRIES = 3;
    protected $signature = 'bpanel4:install-packages';

    protected $description = 'Instala automáticamente los paquetes de bPanel4 compatibles con bittacora/bpanel4-autoinstaller';
    protected $failedInstallers = [];

    /**
     * @throws JsonException
     */
    public function handle(): void
    {
        Artisan::call('migrate --force');
        $composerLock = json_decode(file_get_contents(base_path() . '/composer.lock'), true);

        if (!is_array($composerLock['packages'])) {
            throw new RuntimeException('No se pudo leer composer.lock');
        }

        foreach ($composerLock['packages'] as $package) {
            $this->runPackageInstallers($package);
        }

        /*
         * Como habrá paquetes que dependan de otros y pueden fallar si no se han instalado previamente los paquetes
         * de los que dependen, reintentamos instalar los paquetes que han fallado en varias pasadas.
         */
        for ($i = 0; $i < self::NUMBER_OF_RETRIES; ++$i) {
            foreach ($this->failedInstallers as $failedInstaller) {
                $this->retryCommand($failedInstaller);
            }
        }

        if (!empty($this->failedInstallers)) {
            $this->error(
                'Algunos instaladores no pudieron ejecutarse: ' . implode(', ', $this->failedInstallers)
            );
        } else {
            $this->comment('Todos los instaladores se ejecutaron correctamente');
        }
    }

    private function runPackageCommand(string $command): void
    {
        if (!$this->commandHasAlreadyBeenRun($command)) {
            $this->comment('Ejecutando ' . $command);
            try {
                Artisan::call($command);
                DB::table(self::TABLE)->insert(['command' => $command, 'created_at' => new DateTime()]);
            } catch (\Exception) {
                $this->failedInstallers[] = $command;
            }
            return;
        }
        $this->line('El comando ' . $command . ' ya se había ejecutado');
    }

    private function retryCommand(string $command): void
    {
        if (!$this->commandHasAlreadyBeenRun($command)) {
            $this->comment('Reintentando ejecutar ' . $command . ' porque había fallado previamente');
            try {
                Artisan::call($command);
                DB::table(self::TABLE)->insert(['command' => $command, 'created_at' => new DateTime()]);
                unset($this->failedInstallers[array_search($command, $this->failedInstallers)]);
                $this->comment('OK');
            } catch (\Exception $exception) {
                return;
            }
            return;
        }
    }

    private function commandHasAlreadyBeenRun(string $command): bool
    {
        return DB::table(self::TABLE)
                ->where('command', '=', $command)->count() > 0;
    }

    /**
     * @param array{extra: array{bpanel4: array{installers: string[]}}} $package
     * @return void
     */
    private function runPackageInstallers(array $package): void
    {
        if (isset($package['extra']['bpanel4']['installers'])) {
            $this->info('Ejecutando instaladores pendientes para el paquete ' . $package['name']);
            foreach ($package['extra']['bpanel4']['installers'] as $command) {
                $this->runPackageCommand($command);
            }
        }
    }
}
