<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Autoinstaller;

use Bittacora\Bpanel4\Autoinstaller\Commands\AutoinstallPackages;
use Illuminate\Support\ServiceProvider;

class AutoinstallerServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->commands([AutoinstallPackages::class]);
    }
}
